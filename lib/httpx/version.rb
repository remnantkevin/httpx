# frozen_string_literal: true

module HTTPX
  VERSION = "0.22.5"
end
